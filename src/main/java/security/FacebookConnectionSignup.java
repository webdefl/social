package security;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.stereotype.Service;
import persistence.dao.UserRepository;
import persistence.model.User;

@Service
public class FacebookConnectionSignup implements ConnectionSignUp {

    @Autowired
    private UserRepository userRepository;

    @Override
    public String execute(Connection<?> connection) {
        System.out.println("signup === ");
        String facebookId = connection.getKey().getProviderUserId();
        if(userRepository.findByFacebookId(facebookId) != null){
            return connection.getDisplayName();
        }
        final User user = new User();
        user.setUsername(connection.getDisplayName());
        user.setPassword(randomAlphabetic(8));
        user.setFacebookId(connection.getKey().getProviderUserId());

        userRepository.save(user);
        System.out.println(userRepository.findAll());
        System.out.println(connection.getKey().getProviderUserId());
        return user.getUsername();
    }

}